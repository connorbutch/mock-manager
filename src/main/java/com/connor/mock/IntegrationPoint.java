package com.connor.mock;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * This is an interceptor annotation used to annotate a call to an external webservice.
 * During integration tests, the interceptor should intercept this call, and return the
 * global level mock AND NOT let the invocation context proceed.
 * This should be placed by code generation plugin on all client methods.
 * @author connor
 *
 */
@InterceptorBinding
@Inherited
@Target({METHOD, TYPE}) //NOTE: I would prefer to only use on method, but allow on type for flexibility
@Retention(RUNTIME)
public @interface IntegrationPoint {
	//intentionally blank
}
