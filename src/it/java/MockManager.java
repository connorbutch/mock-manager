

import java.util.Optional;


/**
 * 
 * @author connor
 *
 * @param <B> Base class type
 */
public interface MockManager<B> {

//	/**
//	 * NOTE: this is included for utility purposes only; you should likely never need to use this; as this should autodiscover
//	 * and register the mocks
//	 * @param <T> The type of class extending the base class (B)
//	 * @param clazz
//	 */
//	@Deprecated //this method should not be used, this class should automatically discover all mocked instances, and even if it doesn't then the call to get should create one
//	<T extends B> void registerType(Class<T> clazz);
	
	/**
	 * 
	 * @param <T>  The type of class extending the base class (B)
	 * @param clazz
	 * @return optional -- if value is present for class, then returns it
	 */
	<T extends B> T getMockedInstance(Class<T> clazz);
}
