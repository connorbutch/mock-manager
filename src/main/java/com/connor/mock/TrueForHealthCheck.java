package com.connor.mock;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface TrueForHealthCheck {
	//this is a marker interface that can be used in front of a boolean parameter to indicate the passed value should be true.  Please note, you can also name your parameter "isHealthCheck" or override the ArgumentFactoryDefaultImpl with a specializing bean to do the same thing.
}
