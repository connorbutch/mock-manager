package com.connor.mock;

import java.lang.reflect.Method;
import java.util.Set;

public interface IntegrationPointCollector {

	/**
	 * This method is used to get all references to methods annotated with @IntegrationPoint
	 * @return
	 */
	Set<Method> getIntegrationPoints();
	
}
