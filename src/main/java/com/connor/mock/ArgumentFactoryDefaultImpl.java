package com.connor.mock;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.CDI;

/**
 * This class would be generated, and would provide default arguments for all of the 
 * integration point methods.  
 * 
 * The argument values are gotten with the below steps:
 * NOTE: if value is a primitive, simply get the default value for a primitive
 * 
 * 1 - attempt to get arg value with cdi
 * 2 - invoke the constructor with the least amount of args
 *          if there are args required, then get them by repeating step 2
 * 3 - if cannot get in any of the aboe methods, then return the default value (which is null for objects)
 * 
 * However, since it is gotten through cdi, if you need to 
 * change the arguments passed, then please override it using @Specializes annotation
 * 
 * 
 * NOTE: if you would like, someone can contribute by checking if is a singleton check (as third step)
 * or a builder pattern check (fourth step)
 * @author connor
 *
 */
public class ArgumentFactoryDefaultImpl implements ArgumentFactory {

	/**
	 * If parameter has this name, then pass as true
	 */
	protected static final String HEALTH_CHECK_PARAM_NAME = "isHealthCheck";

	@Override
	public Object[] getArgsForMethod(Method method) {
		Objects.requireNonNull(method, "Method cannot be null");
		Objects.requireNonNull(method.getParameters(), "Parameters cannot be null (can be empty array, but cannot be null reference)");		

		Parameter[] parameters = method.getParameters();

		return Arrays
				.stream(parameters)
				.filter(parameter -> { return parameter != null; })
				.map(this::getValue)
				.collect(Collectors.toList())
				.toArray();
	}

	/**
	 * NOTE: In this case, we may not need the required arg value; can always let people override
	 * @param parameter
	 * @return
	 */
	protected Object getValue(Parameter parameter) {		
		Objects.requireNonNull(parameter, "Parameter passed cannot ever be null");
		Object value = getObjectForClass(parameter.getType());
		return value;
	}

	/**
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	protected <T> T getObjectForClass(Class<T> clazz) {
		Objects.requireNonNull(clazz, "Class must not be null");

		//first try to get with cdi if possible
		T toReturn = getWithCdiIfPossible(clazz).orElseGet(() -> {
			//then try to get with invoking constructors
			return getWithConstructorIfPossible(clazz).orElseGet(() -> {
				//get from static method if possible
				return getFromStaticMethodIfPossible(clazz).orElseGet(() -> {
					//get from builder pattern if it exists
					return getFromBuilderPatternIfPossible(clazz).orElseGet(() -> {
						//if none of the other methods work, then return the default value (this is where you would do singleton check or builder check)
						return getDefaultValue(clazz);
					});					
				});				
			});
		});		

		return toReturn;
	}

	/**
	 * Attempts to get instance of class using cdi.  If this is not possible, it returns null.
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked") //NOTE: none of these are really unchecked, they are all safe
	protected <T> Optional<T> getWithCdiIfPossible(Class<T> clazz){
		Objects.requireNonNull(clazz, "Class must not be null");

		T toReturn = null;

		try {
			Bean<T> bean = (Bean<T>) CDI.current().getBeanManager().getBeans(clazz).iterator().next();
			CreationalContext<T> ctx = CDI.current().getBeanManager().createCreationalContext(bean);
			toReturn = (T) CDI.current().getBeanManager().getReference(bean, clazz, ctx);			
		}catch(Exception e) {
			//intentionally silence (in functional programming manner)
		}

		return Optional.ofNullable(toReturn);
	}

	/**
	 * Attempts to get instance of class, invoking constructor.  If cannot pass, then returns empty optional.
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	protected <T> Optional<T> getWithConstructorIfPossible(Class<T> clazz){
		Objects.requireNonNull(clazz, "Class must not be null");

		//get the constructors for a class
		@SuppressWarnings("unchecked")
		Constructor<T>[] constructors = (Constructor<T>[])clazz.getConstructors();

		//sort them based on number of arguments (less constructors come first)
		Arrays.sort(constructors, (constructor1, constructor2) -> {
			return constructor1.getParameterCount() - constructor2.getParameterCount();
		});

		//loop over constructors (in order)
		return Arrays.stream(constructors)
				.map(constructor -> {
					//transform each constructor to an instance of the class
					T toReturn = null;
					try {
						if(constructor.getParameterCount() > 0) {
							//if there are args, then get the args with recursion	
							Object[] args = Arrays.stream(constructor.getParameters())
									.map(parameter -> parameter.getType())
									.map(this::getObjectForClass)
									.collect(Collectors.toList())
									.toArray();
							toReturn = constructor.newInstance(args);
						}else {
							//if no args for the constructor, then invoke the no arg constructor
							toReturn = constructor.newInstance();
						}
					}catch(Exception e) {
						//intentionally silence and return null in functional programming style
					}

					return toReturn;
				})
				//in true functional programming style, we like functions to be truly "functional" and only use return values, so filter null values
				.filter(value -> { return value != null; })
				.findFirst();
	}

	/**
	 * Gets from static method (such as singleton), if possible.
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked") //NOTE: none of these are really unchecked, they are all safe
	protected <T> Optional<T> getFromStaticMethodIfPossible(Class<T> clazz){
		Objects.requireNonNull(clazz, "Class must not be null");		

		return Arrays.stream(clazz.getMethods())
		.filter(method -> {	return Modifier.isStatic(method.getModifiers()) && method.getReturnType() == clazz;	})
		.findFirst()
		.map(method -> {
			T returnValue = null;
			if(method.getParameterCount() > 0) {
				Object[] args = Arrays.stream(method.getParameters())
						.map(parameter -> {
							return getObjectForClass(parameter.getType());
						})
						.collect(Collectors.toList())
						.toArray();
				try {
					returnValue = (T) method.invoke(null, args);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					//intentionally silence
				}
			}else {
				try {
					returnValue = (T) method.invoke(null);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					//intentionally silence
				}
			}
			return returnValue;
		});
	}
	
	/**
	 * Gets from builder pattern in possible
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	protected <T> Optional<T> getFromBuilderPatternIfPossible(Class<T> clazz){
		return Optional.ofNullable(null); //TODO
	}
	

	/**
	 * Returns the default (jvm) value for the class passed
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected <T> T getDefaultValue(Class<T> clazz){
		Objects.requireNonNull(clazz, "Class must not be null");

		//instantiate an array of that type, then grab the first value for the default type
		return (T) Array.get(Array.newInstance(clazz, 1), 0);
	}
}