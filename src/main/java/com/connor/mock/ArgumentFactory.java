package com.connor.mock;

import java.lang.reflect.Method;

public interface ArgumentFactory {

	/**
	 * 
	 * @param method
	 * @return
	 */
	Object[] getArgsForMethod(Method method);
	
}
