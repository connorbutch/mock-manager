package com.connor.mock;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 * This class exists to mantain references to all annotated methods
 * @author connor
 *
 */
@ApplicationScoped
public class IntegrationPointCollectorImpl implements IntegrationPointCollector {

	/**
	 * 
	 */
	private Set<Method> annotatedMethods;


	@Override
	public Set<Method> getIntegrationPoints() {
		return annotatedMethods;
	}

	/**
	 * This runs on startup
	 * @param o not needed
	 */
	public void collectMethodsOnServerStartup(@Observes @Initialized(ApplicationScoped.class) Object o) {
		System.out.println("Collecting mocks");
		Reflections reflection = new Reflections(new ConfigurationBuilder().setUrls(ClasspathHelper.forPackage("com.connor")).setScanners(new MethodAnnotationsScanner()));
		annotatedMethods = Collections.unmodifiableSet(reflection.getMethodsAnnotatedWith(IntegrationPoint.class));
		System.out.println("Number of annotated methods is: " + annotatedMethods.size());
	}

	/**
	 * 
	 * NOTE: while we could just annotate this method with @PreDestroy, this can lead to issue in an application where
	 * there are multiple application scoped beans
	 * @param o
	 */
	public void cleanupOnShutdown(@Observes @Destroyed(ApplicationScoped.class) Object o) {
		//do cleanup here (if any is deemed necessary)
	}
}