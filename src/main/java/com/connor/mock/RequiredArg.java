package com.connor.mock;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The goal of this class was originally to provide an annotation to put in front of 
 * a parameter at an integration point if we wanted to provide a different value (other
 * than the default) at runtime.  For example, if we wanted to pass a an instance of CustomObject
 * which has a default of null, we could simply have the codegeneration generate this 
 * annotation instead.  However, the only time when this should be required is for path parameters
 * which should almost always be ints (maybe strings or longs once in a while).  In this case,
 * we can provide default values.
 * 
 * However, this all fell to pot when I realized java annotations only allow primitives, Strings, enum, type, class
 * annotation, or an array of these.
 * @author connor
 *
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredArg {
	
	//This isn't allowed by java :(
	//Object defaultValue();
	
	String defaultString() default "";
	
	int defaultInt() default 0;
	
	long defaultLong() default 0;
	
	
}
