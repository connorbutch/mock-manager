package com.connor.mock;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * This exists to be used as a qualifier when the regular {@link @Alternative} annotation
 * cannot be used
 * @author connor
 *
 */
@Target({METHOD, TYPE}) //NOTE: I would prefer to only use on method, but allow on type for flexibility
@Qualifier
@Retention(RUNTIME)
public @interface CustomAlternative {
	//intentionally blank
}
