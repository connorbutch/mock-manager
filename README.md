# mock-manager

As those of you who know me know, I am passionate about CI/CD.  One key element
that must be handled when doing CI/CD is that of **integration points**.  What do
I mean when I say integration points?  These are methods that call other 
applications/external resources (except databases, which should always be 
handled with test containers).  Examples of such integration points include
calling other webservices, reading from a kafka topic, reading from queue, etc.
Connecting technologies (usually in the form of code generation), should almost 
always be used for these integration points.


These points must be treated in a special manner, both when running integration
tests, and when deploying your application.  As any company that is truly mature
with CI/CD knows, CI/CD must involve integration tests (and I would propose never
using end-to-end tests) which are (effectively) 100% reliable.  This means external
integrations should be mocked (at integration points).  In order to handle this
this project does the following:
*  Annotates all integration points with @MockForIntegrationTest
*  Creates a singleton for registering mocks (for classes marked with methods annotated with above mock)
*  Creates an interceptor for methods annotated with this, and calls the method on the mock instead of the real class (using reflection); the result of the mock will be passed to caller (be it exception or return value) and the "real" method is never invoked

In order to use this project, one should simply inject their MockManager (or get by calling getInstance), and use
Mockito to mock the method calls (i.e. use the following
Mockito.when(mockManager.getMockForClass(ClassContainingIntPoint.class)).thenReturn("")
) *before running integration tests*


However, connectivity between applications must also be tested for each integration
point.  For example, this means that our application must be able to call each
method provided by the webservices it calls (using generated code), or our application
should be able to select from our database.  As we do not want external factors
interfering with our integration tests, when we first deploy, we have not tested
the connectivity between applications.  Thus, the first thing we do after a 
deployment is check that our system can communicate and use every integration point.
Hence, the integration health check does the following
*  On server startup: finds (and collects) all methods annotated with @MockForIntegrationTest
*  When integration health check is invoked, it goes ahead and calls the ArgumentFactory to get the args for the method 
*  The argument factory provides the default value for the method (0 for int, false for boolean, null for complex types, etc)
*  NOTE: have thought about having an annotation @RequiredArg with object for default value for tests, but java doesn't allow Object in annotation
*  If you wish to override this behavior, declare a specializing bean (annotated with @Specializes) that extends ArgumentFactoryDefaultImpl
