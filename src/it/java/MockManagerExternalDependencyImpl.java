

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.mockito.Mockito;

import com.connor.mock.CustomAlternative;
import com.connor.ws.ExternalDependency;

/**
 * This class is responsible for hold
 * @author connor
 *
 */
//@Singleton //since we use this inside of interceptor, cdi is not allowed (either field or constructor level injection), so use regular singleton
@CustomAlternative //if you mark with alternative, doesn't discover producer as well, so make custom qualifier
public class MockManagerExternalDependencyImpl implements MockManager<ExternalDependency> {

	/**
	 * Thread safe singleton (eager instantiation)
	 */
	private static final MockManagerExternalDependencyImpl INSTANCE = new MockManagerExternalDependencyImpl();
	
	/**
	 * System property if we should enable auto discovery
	 */
	private static final String ENABLE_AUTO_SYS_PROPERTY = "ENABLE_AUTO_DISCOVERY";
	
	/**
	 * Weld (our cdi provider) works internally by subclassing the bean, and giving it its own class, which always ends in this string
	 * for example: com.connor.ws.ExternalDependencyPolicyClientImpl$Proxy$_$$_WeldSubclass
	 */
	private static final String WELD_START_OF_CLASS_NAME = "$Proxy";

	/**
	 * Our lock for thread-safety
	 */
	private final Object LOCK = new Object();

	/**
	 * Note: we know this is actually Map<Class<? extends ApiClient>, ? extends ApiClient>, where both ? are the same
	 */
	private final Map<Class<? extends ExternalDependency>, Object> map;

	//@Inject
//	public MockManagerExternalDependencyImpl(Instance<ExternalDependency> externalDependencies) {
//		map = new HashMap<>(); //TODO maybe use synchronized map implementation
//
//		if(Boolean.TRUE.toString().equals(System.getProperty(ENABLE_AUTO_SYS_PROPERTY))){
//			externalDependencies
//			.stream()
//			.map(apiClient -> { //TODO maybe make this a method reference?
//				return apiClient.getClass();
//			})
//			.forEach(this::registerType);
//		}
//	}		
	
	private MockManagerExternalDependencyImpl() {
		map = new HashMap<>();
	}
	
	/**
	 * Note: while I prefer cdi singleton, we are in a case where we cannot use cdi, so this allows cdi usage, and usage from traditional java
	 * to get reference to same object
	 * @return
	 */
	@Produces
	public static MockManagerExternalDependencyImpl getInstance() {
		return INSTANCE;
	}

	@SuppressWarnings("unchecked") //NOTE: since we are only ones adding to map, we know the type of the object
	@Override
	public <T extends ExternalDependency> T getMockedInstance(Class<T> clazz) { 
		System.out.println("Getting instance of class " + clazz.getName());
		
		//Weld (our cdi provider) works internally by subclassing and giving a name ending in $PROXY, so trim that
		int indexOfWeldStart = clazz.getName().lastIndexOf(WELD_START_OF_CLASS_NAME);
		if(indexOfWeldStart != -1) {
			String classNameWithoutWeld = clazz.getName().substring(0, indexOfWeldStart);
			System.out.println("Class without weld: " + classNameWithoutWeld);
			try {
				//we know we can cast here, because weld will provide a subclass
				clazz = (Class<T>)Class.forName(classNameWithoutWeld);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Weird issue with cdi naming of proxied beans", e);
			}
		}

		//NOTE: must synchronize, as this is a singleton (I know could be slightly more efficient, but i would suspect never have two threads hit this at once)
		synchronized (LOCK) {
			if(!map.containsKey(clazz)) {
				registerType(clazz);
			}
		}

		return (T) map.get(clazz);
	}

	protected <T extends ExternalDependency> void registerType(Class<T> clazz) {
		System.out.println("Map doesn't contain type, so adding it");
		map.putIfAbsent(clazz, Mockito.mock(clazz));		
	}
}