

import java.lang.reflect.Method;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.connor.mock.IntegrationPoint;
import com.connor.ws.ExternalDependency;


/**
 * This class holds the interceptors
 * @author connor
 *
 */
@Interceptor
@IntegrationPoint
@Priority(Interceptor.Priority.APPLICATION) //NOTE: this is required if we are not registering this in beans.xml, and is also required if we want to apply the interceptor across multiple modules (jars/wars)
public class IntegrationPointInterceptor {

	//@Inject
	private MockManagerExternalDependencyImpl mockManager = MockManagerExternalDependencyImpl.getInstance(); //TODO figure out why cdi (either constructor or field injection) doesn't work here
//	
//	/**
//	 * Cdi-enabled constructor
//	 * @param mockManager
//	 */
//	@Inject
//	public IntegrationTestInterceptor(MockManagerExternalDependencyImpl mockManager) {
//		this.mockManager = mockManager;
//	}

	/**
	 * This method intercepts external calls for integration tests, and passes them to the mock
	 * @param context
	 * @throws Exception
	 */
	@AroundInvoke
	public Object interceptAndMockExternalCall(InvocationContext context) throws Exception {
		System.out.println("Inside InterceptorHolder.interceptAndMockExternalCall");
		
		//get what we can from the 
		Class<?> clazz = context.getTarget().getClass();
		String methodName = context.getMethod().getName();
		Object[] argsForMethod = context.getParameters();
		
		//loop through parameters and create an array of their types for use with reflection later
		Class<?>[] parameterTypes = new Class<?>[argsForMethod.length];
		for(int i = 0; i < argsForMethod.length; ++i) {
			parameterTypes[i] = argsForMethod[i].getClass();
		}		
		
		//this means that this annotation was used on an incorrect method/class that does not implement our marker interface
		if(!ExternalDependency.class.isAssignableFrom(clazz)) {
			throw new Exception("Please only annotate methods with mockforintegration test interceptor binding if the class implements the marker interface ApiClient");
		}
		
		//we know this is assignable, so we can cast here
		//get our mock from the singleton mock manager
		@SuppressWarnings("unchecked") //we know this is safe to cast because of the assignable from check above
		Object mockToInvoke = mockManager.getMockedInstance((Class<? extends ExternalDependency>) clazz);
		
		//get the method to invoke on the mock from the method name and parameters
		Method methodToInvoke = mockToInvoke.getClass().getMethod(methodName, parameterTypes);
		Object returnValue;
		try {
			returnValue = methodToInvoke.invoke(mockToInvoke, argsForMethod);
		}catch(Exception e) {
			//TODO
			//NOTE: break out into separate catch blocks for each type -- original method throws exception, then rethrow
			throw new Exception("There was an error using reflection to invoke method on mock", e);
		}
		
		//log success here
		System.out.println(returnValue);
		return returnValue;	
	}	
}