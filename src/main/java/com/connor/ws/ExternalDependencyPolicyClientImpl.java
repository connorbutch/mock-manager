package com.connor.ws;

import com.connor.mock.IntegrationPoint;

public class ExternalDependencyPolicyClientImpl implements ExternalDependency {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8845199637628066567L;

	/**
	 * This is a dummy method
	 * @return
	 */
	@IntegrationPoint
	public String dummy() {
		return "dummy";
	}
}
