import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.mockito.Mockito;

import com.connor.ws.ExternalDependency;
import com.connor.ws.ExternalDependencyPolicyClientImpl;

public class IntegrationTestDriver {

	public static void main(String[] args) {
		Weld weld = new Weld();
		//NOTE: neither of these seem to work . . . 
		//weld.addBeanClass(IntegrationPointInterceptor.class);
		//weld.addInterceptor(IntegrationPointInterceptor.class);
		weld.enableInterceptors(IntegrationPointInterceptor.class);
		try(WeldContainer cdiContainer = weld.initialize()){
			MockManagerExternalDependencyImpl mockManager = cdiContainer.select(MockManagerExternalDependencyImpl.class).get();
			ExternalDependencyPolicyClientImpl client = mockManager.getMockedInstance(ExternalDependencyPolicyClientImpl.class);
			Mockito.when(client.dummy()).thenReturn("changed value");
			System.out.println(client.dummy());
			ExternalDependencyPolicyClientImpl externalPolicyClient = cdiContainer.select(ExternalDependencyPolicyClientImpl.class).get();
			System.out.println(externalPolicyClient.dummy());
		}
	}


}
