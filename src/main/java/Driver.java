
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;


import com.connor.mock.ArgumentFactory;
import com.connor.mock.IntegrationPointCollector;


public class Driver {

	public static void main(String[] args) {
		System.out.println("Started main");

		try(WeldContainer cdiContainer = new Weld().initialize()){
			System.out.println("Successfully initialized cdi container");

			//NOTE: This wouldn't be in same project as interceptor
			System.out.println("Invoking all methods annotated with @Integration point");
			//collect all integration points
			IntegrationPointCollector collector = cdiContainer.select(IntegrationPointCollector.class).get();
			Set<Method> integrationPoints = collector.getIntegrationPoints();

			//loop over them, and invoke the methods
			integrationPoints.
			stream()
			.filter(integrationPoint -> { return integrationPoint != null; })
			.forEach(integrationPoint -> {
				//get a class instance containing the method with cdi
				Object toInvokeMethodOn = cdiContainer.select(integrationPoint.getDeclaringClass()).get();
				Object returnValueFromMethod = null; //avoid compiler yelling at us

				//check the number of args
				if(integrationPoint.getParameterCount() > 0) {
					ArgumentFactory argumentFactory = cdiContainer.select(ArgumentFactory.class).get();
					Object[] argsForMethod = argumentFactory.getArgsForMethod(integrationPoint);
					try {
						returnValueFromMethod = integrationPoint.invoke(toInvokeMethodOn, argsForMethod);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {					
					//handle no args
					try {
						returnValueFromMethod = integrationPoint.invoke(toInvokeMethodOn);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(integrationPoint.getReturnType() != void.class) {
					System.out.println("Value returned from integration point method is " + returnValueFromMethod);
				}else {
					System.out.println("Method invoked was a void method");
				}

		

			});
		}



		System.out.println("End app");
	}
}